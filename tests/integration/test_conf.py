import json
import subprocess
import sys
import tempfile

import yaml


def test_config(runpy):

    # Create a config template
    proc_template = subprocess.run(
        [sys.executable, runpy, "--config-template"],
        capture_output=True,
        check=True,
        encoding="ascii",
        env={},
    )
    # Verify that output was created
    assert proc_template.stdout, proc_template.stderr
    # Verify that the output is valid yaml
    assert yaml.safe_load(proc_template.stdout)

    with tempfile.NamedTemporaryFile(suffix=".cfg", delete=True, mode="w+") as fh:
        # Write the config template to a config file
        fh.write(proc_template.stdout)
        fh.flush()

        # Run an idem process with the --config flag after the subcommand
        proc = subprocess.run(
            [
                sys.executable,
                runpy,
                "exec",
                "test.ping",
                "--output=json",
                f"--config={fh.name}",
            ],
            capture_output=True,
            encoding="ascii",
            env={},
        )
        assert proc.stdout, proc.stderr
        assert json.loads(proc.stdout) == {
            "comment": None,
            "ref": "exec.test.ping",
            "result": True,
            "ret": True,
        }

        # Run an idem process with the --config flag before the subcommand
        proc = subprocess.run(
            [
                sys.executable,
                runpy,
                f"--config={fh.name}",
                "exec",
                "test.ping",
                "--output=json",
            ],
            capture_output=True,
            encoding="ascii",
            env={},
        )
        assert proc.stdout, proc.stderr
        assert json.loads(proc.stdout) == {
            "comment": None,
            "ref": "exec.test.ping",
            "result": True,
            "ret": True,
        }
